<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="node_modules/@fortawesome/fontawesome-free/css/all.min.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.css">
    <title>PNRUPLANT</title>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
</head>

<body>

    <!-- Section Navbar -->
    <nav id="navbar" class="navbar navbar-expand-lg fixed-top navbar-dark bg-alpha">
        <div class="container">
            <a class="navbar-brand" href="#">
                <img src="assets/image/logo.jpg" width="35" height="35" class="d-inline-block align-top" alt="">
                PNRUPLANT
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarKey" aria-controls="navbarKey" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarKey">
                <ul class="navbar-nav ml-auto text-center">
                    <li class="nav-item active">
                        <a class="nav-link" href="#home">หน้าหลัก <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#footer">เกี่ยวกับเรา</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#input">เพิ่มข้อมูล</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#all">รายชื่อพรรณไม้ทั้งหมด</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <br><br><br><br><br>
    <!--vueja-->
    <section>
        <div class="container">
            <h2>เพิ่มข้อมูล</h2>
            <div class="col-md-12">
                <a href="insertdataplant.php" class="btn btn-primary">เพิ่มข้อมูลตารางplant</a>
                <a href="insertdataarea.php" class="btn btn-primary">เพิ่มข้อมูลตารางarea</a>
                <a href="showdata.php" class="btn btn-primary">ดูข้อมูลทั้งหมด</a>
                <a href="showdatalocation.php" class="btn btn-primary">ดูพิกัดทั้งหมด</a>
            </div>
        </div>
    </section>
    <div>
        <br><br> 
    </div>
    <!--endvue-->
    <!-- insert -->

    <div class="container">
        <form action="insertvalueplantdetail.php" method="post">
            <div class="form-row">
                <!-- PlantID -->
                <div class="form-group col-md-6">
                    <label for="PlandetailtID">PlandetailtID</label>
                    <input type="text" class="form-control" name="PlandetailtID">
                </div>
                <!-- PlantName -->
                <div class="form-group col-md-6">
                    <label for="inputEmail4">PlantName</label>
                    <input type="text" class="form-control" name="PlantName">
                </div>
                <!-- PlantScience -->
                <div class="form-group col-md-6">
                    <label for="inputEmail4">PlantScience</label>
                    <input type="text" class="form-control" name="PlantScience">
                </div>
                <!-- PlantDiscover -->
                <div class="form-group col-md-6">
                    <label for="PlantDiscover">PlantDiscover</label>
                    <input type="text" class="form-control" name="PlantDiscover">
                </div>
            </div>
            <!-- PlantCommonname -->
            <div class="form-group">
                <label for="PlantCommonname">PlantCommonname</label>
                <input type="text" class="form-control" name="PlantCommonname" placeholder="">
            </div>
            <!-- PlantType -->
            <div class="form-group">
                <label for="PlantType">PlantType</label>
                <input type="text" class="form-control" name="PlantType" placeholder="ประเภท เช่น ไม้ยืนต้น">
            </div>
            <!-- PlantTypeENG -->
            <div class="form-group">
                <label for="PlantTypeENG">PlantTypeENG</label>
                <input type="text" class="form-control" name="PlantTypeEng" placeholder="ประเภท เช่น ไม้ยืนต้น">
            </div>
            <!-- PlantDistrbution -->
            <div class="form-group">
                <label for="PlantDistrbution">PlantDistrbution</label>
                <textarea class="form-control" name="PlantDistrbution" rows="4"></textarea>
            </div>
            <!-- PlantDistrbutionEng -->
            <div class="form-group">
                <label for="PlantDistrbutionEng">PlantDistrbutionEng</label>
                <textarea class="form-control" name="PlantDistrbutionEng" rows="4"></textarea>
            </div>
            <!-- PlantBenefit -->
            <div class="form-group">
                <label for="PlantBenefit">PlantBenefit</label>
                <textarea class="form-control" name="PlantBenefit" rows="4"></textarea>
            </div>
            <!-- PlantBenefitEng -->
            <div class="form-group">
                <label for="PlantBenefitEng">PlantBenefitEng</label>
                <textarea class="form-control" name="PlantBenefitEng" rows="4"></textarea>
            </div>
            <!-- PlantBanefity -->
            <div class="form-group">
                <label for="PlantBanefity">PlantBanefity</label>
                <textarea class="form-control" name="PlantBanefity" rows="4"></textarea>
            </div>
            <!-- PlantBanefityEng -->
            <div class="form-group">
                <label for="PlantBanefityEng">PlantBanefityEng</label>
                <textarea class="form-control" name="PlantBanefityEng" rows="4"></textarea>
            </div>
            <!-- PlantFlower -->
            <div class="form-group">
                <label for="PlantFlower">PlantFlower</label>
                <textarea class="form-control" name="PlantFlower" rows="4"></textarea>
            </div>
            <!-- PlantFlowerEng -->
            <div class="form-group">
                <label for="PlantFlowerEng">PlantFlowerEng</label>
                <textarea class="form-control" name="PlantFlowerEng" rows="4"></textarea>
            </div>
            <!-- PlantRound -->
            <div class="form-group">
                <label for="PlantRound">PlantRound</label>
                <textarea class="form-control" name="PlantRound" rows="4"></textarea>
            </div>
            <!-- PlantRoundEng -->
            <div class="form-group">
                <label for="PlantRoundEng">PlantRoundEng</label>
                <textarea class="form-control" name="PlantRoundEng" rows="4"></textarea>
            </div>
            <!-- PlantSeed -->
            <div class="form-group">
                <label for="PlantSeed">PlantSeed</label>
                <textarea class="form-control" name="PlantSeed" rows="4"></textarea>
            </div>
            <!-- PlantSeedEng -->
            <div class="form-group">
                <label for="PlantSeedEng">PlantSeedEng</label>
                <textarea class="form-control" name="PlantSeedEng" rows="4"></textarea>
            </div>
            <!-- PlantStem -->
            <div class="form-group">
                <label for="PlantStem">PlantStem</label>
                <textarea class="form-control" name="PlantStem" rows="4"></textarea>
            </div>
            <!-- PlantStemEng -->
            <div class="form-group">
                <label for="PlantStemEng">PlantStemEng</label>
                <textarea class="form-control" name="PlantStemEng" rows="4"></textarea>
            </div>
            <!-- PlantLeaf -->
            <div class="form-group">
                <label for="PlantLeaf">PlantLeaf</label>
                <textarea class="form-control" name="PlantLeaf" rows="4"></textarea>
            </div>
            <!-- PlantLeafEng -->
            <div class="form-group">
                <label for="PlantLeafEng">PlantLeafEng</label>
                <textarea class="form-control" name="PlantLeafEng" rows="4"></textarea>
            </div>

            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="inputSeasonID">SeasonID</label>
                    <select name="inputSeasonID" class="form-control">
                        <option selected>Choose...</option>
                        <option>...</option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="inputPlantfamilyID">PlantfamilyID</label>
                    <select name="inputPlantfamilyID" class="form-control">
                        <option selected>Choose...</option>
                        <option>...</option>
                    </select>
                </div>
            </div>
            
            <button type="submit" class="btn btn-primary">Sign in</button>
        </form>
    </div>

    <!-- endinsert -->
    <!--contact-->
    <div class="jumbotron jumbotron-fluid mt-5 p-5 text-center text-md-left">
        <div class="row">
            <div class="col-md-4">
                <a class="navbar-brand" href="#">
                    <img src="assets/image/logo.jpg" width="35" height="35" class="d-inline-block align-top" alt="">
                    AppzStory Studio
                </a>
                <p>
                    <i class="fa fa-phone-square"></i> 09-999-9999 <br>
                    <i class="fa fa-envelope"></i> email@example.com <br>
                    <i class="fa fa-address-card"></i> Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa,
                    aspernatur!
                </p>
                <a href="https://www.facebook.com/WebAppzStory" target="_blank">
                    <i class="fab fa-facebook-square fa-2x"></i>
                </a>
                <a href="https://www.youtube.com/appzstorystudio" target="_blank">
                    <i class="fa fa-youtube-square fa-2x"></i>
                </a>
            </div>
            <div class="col-md-3">
                <h4>เมนู</h4>
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="#home">หน้าหลัก <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#footer">เกี่ยวกับเรา</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#input">เพิ่มข้อมูล</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#all">รายชื่อพรรณไม้ทั้งหมด</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-5">
                <h4>แผนที่</h4>
                <div id="map"></div>
            </div>
        </div>
    </div>
    <!--endcontact-->
    <!-- Blog -->

    <!-- Section Footer -->
    <!--
    <footer class="semi-footer mt-5 p-5 text-center text-md-left" id="footer">
        <div class="row">
            <div class="col-md-4">
                <a class="navbar-brand" href="#">
                    <img src="assets/image/logo.jpg" width="35" height="35" class="d-inline-block align-top" alt="">
                    AppzStory Studio
                </a>
                <p>
                    <i class="fa fa-phone-square"></i> 09-999-9999 <br>
                    <i class="fa fa-envelope"></i> email@example.com <br>
                    <i class="fa fa-address-card"></i> Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa,
                    aspernatur!
                </p>
                <a href="https://www.facebook.com/WebAppzStory" target="_blank">
                    <i class="fab fa-facebook-square fa-2x"></i>
                </a>
                <a href="https://www.youtube.com/appzstorystudio" target="_blank">
                    <i class="fa fa-youtube-square fa-2x"></i>
                </a>
            </div>
            <div class="col-md-3">
                <h4>เมนู</h4>
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="#home">หน้าหลัก <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#footer">เกี่ยวกับเรา</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#input">เพิ่มข้อมูล</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#all">ราชื่อพรรณไม้ทั้งหมด</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-5">
                <h4>แผนที่</h4>
                <div id="map"></div>
            </div>
        </div>
    </footer>
    
-->
    <footer class="footer">
        <span> COPYRIGHT © 2020
            <a href="#" target="_blank">Soymilk</a>
            ALL Right Reserved
        </span>
    </footer>

    <script>
        var x = document.getElementById("demo");

        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                x.innerHTML = "Geolocation is not supported by this browser.";
            }
        }

        function showPosition(position) {
            x.innerHTML = "Latitude: " + position.coords.latitude +
                "  Longitude: " + position.coords.longitude;
        }
    </script>
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="assets/js/main.js"></script>

</body>

</html>