<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <title>ระบบค้นหาพรรณไม้ PNRU</title>
</head>

<body>
    <nav class="navbar navbar-dark bg-dark">
        <a class="navbar-brand" href="#">หน้ากรอกข้อมูลพรรณไม้ระบบ</a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarsExample01"
            aria-controls="navbarsExample01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="navbar-collapse collapse" id="navbarsExample01" style="">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">ดูรายชื่อทั้งหมด <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="#">admin <span class="sr-only">(current)</span></a>
                </li>
        </div>
        </li>
        </ul>
        </div>
    </nav>
    <div class="container">
        <form action="insertvaluearea.php" method="post">
            <div class="form-group">
                <!-- ZoneID -->
                <label for="exampleFormControlInput1">ใส่รหัส ZoneID </label>
                <input type="text" class="form-control" name="ZoneID" placeholder="เช่น CB01">
            </div>
            <div class="form-group">
                <!-- PlantID -->
                <label for="exampleFormControlInput1">ใส่รหัส PlantID</label>
                <input type="text" class="form-control" name="PlantID" placeholder="63201-10220-99-001/1">
            </div>
            <div class="form-group">
                <!-- longtitudeY lat -->
                <label for="exampleFormControlInput1">longtitudeY</label>
                <input type="text" class="form-control" name="longtitudeY" placeholder="เช่น 13.xxxxxx">
            </div>
            <div class="form-group">
                <!-- latitudeX long -->
                <label for="exampleFormControlInput1">latitudeX</label>
                <input type="text" class="form-control" name="latitudeX" placeholder="เช่น 100.xxxxxx">
            </div>
            <div class="form-group">
                <!-- ชื่อพรรณไม้ -->
                <label for="PlantName">ชื่อพรรณไม้ PlantName</label>
                <input type="text" class="form-control" name="PlantName" placeholder="ภาษาไทย">
            </div>
            <div class="form-group">
                <!-- status -->
                <label for="exampleFormControlInput1">status</label>
                <input type="text" class="form-control" name="status" placeholder="">
            </div>

            <input name="" id="" class="btn btn-primary" type="submit" value="เพิ่มข้อมูลชนิดพรรณไม้">
        </form>
    </div>
    </div>



    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
</body>

</html>