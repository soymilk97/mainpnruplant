<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <title>ระบบค้นหาพรรณไม้ PNRU</title>
</head>

<body>
    <nav class="navbar navbar-dark bg-dark">
        <a class="navbar-brand" href="#">หน้ากรอกข้อมูลพรรณไม้ระบบ</a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarsExample01"
            aria-controls="navbarsExample01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="navbar-collapse collapse" id="navbarsExample01" style="">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">ดูรายชื่อทั้งหมด <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="#">admin <span class="sr-only">(current)</span></a>
                </li>
        </div>
        </li>
        </ul>
        </div>
    </nav>
    <div class="container">
        <form action="insertvaluedetailplant.php" method="post">
            <div class="form-group">
                <!-- PlantID -->
                <label for="PlantID">ใส่รหัส PlantID </label>
                <input type="text" class="form-control" name="PlantID" placeholder="">
            </div>
            <div class="form-group">
                <!-- ชื่อพรรณไม้ -->
                <label for="PlantName">ชื่อพรรณไม้ PlantName</label>
                <input type="text" class="form-control" name="PlantName" placeholder="ภาษาไทย">
                <label for="PlantNameENG">ชื่อพรรณไม้ภาษาอังกฤษ PlantNameENG</label>
                <input type="text" class="form-control" name="PlantNameENG" placeholder="ภาษาอังกฤษ">
            </div>
            <div class="form-group">
                <!-- ชื่อวิทย์ -->
                <label for="PlantScience">ชื่อทางวิทยาศาสตร์ PlantScience</label>
                <input type="text" class="form-control" name="PlantScience" placeholder="">
            </div>
            <div class="form-group">
                <!-- ชื่อผู้ค้นพบ -->
                <label for="PlantDiscover">ชื่อผู้ค้นพบ PlantDiscover</label>
                <input type="text" class="form-control" name="PlantDiscover" placeholder="">
            </div>
            <div class="form-group">
                <!-- ชื่อสามัญ -->
                <label for="PlantCommonname">ชื่อสามัญ PlantCommonname</label>
                <input type="text" class="form-control" name="PlantCommonname" placeholder="">
            </div>
            <div class="form-group">
                <!-- ประเภ?ของพรรณไม้ รอทำลูปข้อมูลประเภท -->
                <label for="PlantType">ประเภทพรรณไม้ PlantType</label>
                <input type="text" class="form-control" name="PlantType" placeholder="เช่น ไม้ยืนต้น">
                <label for="PlantTypeENG">ประเภทพรรณไม้ภาษาอังกฤษ PlantTypeENG</label>
                <input type="text" class="form-control" name="PlantTypeENG" placeholder="">
            </div>
            <div class="form-group">
                <!-- สถานที่ค้นพบ  -->
                <label for="PlantDistrbution">สถานที่ค้นพบ PlantDistrbution</label>
                <textarea class="form-control" name="PlantDistrbution" rows="4"></textarea>
                <label for="PlantDistrbution">สถานที่ค้นพบภาษาอังกฤษ PlantDistrbutionENG</label>
                <textarea class="form-control" name="PlantDistrbutionENG" rows="4"></textarea>
            </div>
            <div class="form-group">
                <!-- ประโชน์ -->
                <label for="PlantDistrbution">ประโยชน์ PlantBenefit</label>
                <textarea class="form-control" name="PlantBenefit" rows="4"></textarea>
                <label for="PlantDistrbution">ประโยชน์ภาษาอังกฤษ PlantBenefitENG</label>
                <textarea class="form-control" name="PlantBenefitENG" rows="4"></textarea>
            </div>
            <div class="form-group">
                <!-- ประโยชน์อื่นๆ -->
                <label for="PlantDistrbution">ประโยชน์อื่นๆ PlantBanefity</label>
                <textarea class="form-control" name="PlantBanefity" rows="4"></textarea>
                <label for="PlantDistrbution">ประโยชน์อื่นๆภาษาอังกฤษ PlantBanefityENG</label>
                <textarea class="form-control" name="PlantBanefityENG" rows="4"></textarea>
            </div>
            <div class="form-group">
                <!-- รายละเอียดลำต้น -->
                <label for="PlantDistrbution">รายละเอียดของลำต้น PlantStem</label>
                <textarea class="form-control" name="PlantStem" rows="4"></textarea>
                <label for="PlantDistrbution">ลำต้นภาษาอังกฤษ PlantStemENG</label>
                <textarea class="form-control" name="PlantStemENG" rows="4"></textarea>
            </div>
            <div class="form-group">
                <!--รายละเอียดใบ-->
                <label for="PlantDistrbution">รายละเอียดของใบ PlantLeaf</label>
                <textarea class="form-control" name="PlantLeaf" rows="4"></textarea>
                <label for="PlantDistrbution">รายละเอียดของใบภาษาอังกฤษ PlantLeafENG</label>
                <textarea class="form-control" name="PlantLeafENG" rows="4"></textarea>
            </div>
            <div class="form-group">
                <!--รายละเอียดดอก-->
                <label for="PlantDistrbution">รายละเอียดของดอก PlantFlower</label>
                <textarea class="form-control" name="PlantFlower" rows="4"></textarea>
                <label for="PlantDistrbution">รายละเอียดของดอกภาษาอังกฤษ PlantFlowerENG</label>
                <textarea class="form-control" name="PlantFlowerENG" rows="4"></textarea>
            </div>
            <div class="form-group">
                <!--รายละเอียดผล-->
                <label for="PlantDistrbution">รายละเอียดของผล PlantRound</label>
                <textarea class="form-control" name="PlantRound" rows="4"></textarea>
                <label for="PlantDistrbution">รายละเอียดของผลภาษาอังกฤษ PlantRoundENG</label>
                <textarea class="form-control" name="PlantRoundENG" rows="4"></textarea>
            </div>
            <div class="form-group">
                <!--รายละเอียดเมล็ด-->
                <label for="PlantDistrbution">รายละเอียดของเมล็ด Seed</label>
                <textarea class="form-control" name="PlantSeed" rows="4"></textarea>
                <label for="PlantDistrbution">รายละเอียดของเมล็ดภาษาอังกฤษ SeedENG</label>
                <textarea class="form-control" name="PlantSeedENG" rows="4"></textarea>
            </div>
            <input name="" id="" class="btn btn-primary" type="submit" value="เพิ่มข้อมูลชนิดพรรณไม้">
        </form>
    </div>
    </div>



    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
</body>

</html>