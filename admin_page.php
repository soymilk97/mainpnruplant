<?php session_start();?>
<?php 

if (!$_SESSION["AdminID"]){  //check session

	  Header("Location: index.html"); //ไม่พบผู้ใช้กระโดดกลับไปหน้า login form 

}else{?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="node_modules/@fortawesome/fontawesome-free/css/all.min.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.css">
    <title>PNRUPLANT</title>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
</head>

<body>

    <!-- Section Navbar -->
    <nav id="navbar" class="navbar navbar-expand-lg fixed-top navbar-dark bg-alpha">
        <div class="container">
            <a class="navbar-brand" href="#">
                <img src="assets/image/logo.jpg" width="35" height="35" class="d-inline-block align-top" alt="">
                PNRUPLANT
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarKey"
                aria-controls="navbarKey" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarKey">
                <ul class="navbar-nav ml-auto text-center">
                    <li class="nav-item active">
                        <a class="nav-link" href="#home">หน้าหลัก <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#footer">เกี่ยวกับเรา</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#input">เพิ่มข้อมูล</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#all">รายชื่อพรรณไม้ทั้งหมด</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Section Carousel -->
    <section id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <a href="#home">
                    <div class="carousel-img"
                        style="background-image: url('https://images.unsplash.com/photo-1486312338219-ce68d2c6f44d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=74be18f074e19e06a51221f0f09969df&auto=format&fit=crop&w=1280&q=80') ;">
                        <div class="carousel-caption">
                            <h1 class="display-4 font-weight-bold">PNRUPLANT</h1>
                            <p class="lead">ค้นหาพรรณไม้</p>
                        </div>
                        <div class="backscreen"></div>
                    </div>
                </a>
            </div>
            <div class="carousel-item">
                <a href="#input">
                    <div class="carousel-img"
                        style="background-image: url('https://images.unsplash.com/photo-1460925895917-afdab827c52f?ixlib=rb-0.3.5&s=d8791fe0f9f4e735158400f9daf6a558&auto=format&fit=crop&w=1280&q=80') ;">
                        <div class="carousel-caption">
                            <h1 class="display-4 font-weight-bold">PNRUPLANT</h1>
                            <p class="lead">บอกต้นไม้ต้นใหม่</p>
                        </div>
                        <div class="backscreen"></div>
                    </div>
                </a>

            </div>
            <div class="carousel-item">
                <a href="#all">
                    <div class="carousel-img"
                        style="background-image: url('https://images.unsplash.com/photo-1488590528505-98d2b5aba04b?ixlib=rb-0.3.5&s=17460aa3d0fd3eb2fb7162edafc37175&auto=format&fit=crop&w=1280&q=80') ;">
                        <div class="carousel-caption">
                            <h1 class="display-4 font-weight-bold">PNRUPLANT</h1>
                            <p class="lead">ดูต้นไม้ทั้งหมด</p>
                        </div>
                        <div class="backscreen"></div>
                    </div>
                </a>
            </div>
            <div class="carousel-item">
                <a href="#footer">
                    <div class="carousel-img" style="background-image: url('assets/image/logo.jpg') ;">
                        <div class="carousel-caption">
                            <h1 class="display-4 font-weight-bold">PNRUPLANT</h1>
                            <p class="lead">ติดต่อเรา</p>
                        </div>
                        <div class="backscreen"></div>
                    </div>
                </a>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </section>
    <!--vueja-->
    <section>
        <div class="container">
            <h2>เพิ่มข้อมูล</h2>
            <div class="col-md-12">
                <!-- Button trigger modal -->
                <a href="insertdataplant.php" class="btn btn-primary">เพิ่มข้อมูลตารางplant</a>
                <a href="insertdataarea.php" class="btn btn-primary">เพิ่มข้อมูลตารางarea</a>
                <a href="showdata.php" class="btn btn-primary">ดูข้อมูลทั้งหมด</a>
                <a href="showdatalocation.php" class="btn btn-primary">ดูพิกัดทั้งหมด</a>
                <a href="insertTBplantdetail.php" class="btn btn-primary">เพิ่มรายละเอียดพรรณไม้</a>
                <a href="logout.php" class="btn btn-danger">Log out</strong></a>
            </div>
        </div>
    </section>
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="assets/js/main.js"></script>

</body>

</html>
<?php }?>