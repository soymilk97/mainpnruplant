<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="node_modules/@fortawesome/fontawesome-free/css/all.min.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.css">
    <title>รายละเอียดข้อมูลพรรณไม้</title>
</head>
<body>

    <div class="container">
    <h1>แสดงข้อมูล</h1>
    <?php
    //รับ parameter มาเก็บในตัวแปร ID
    $ID=$_GET["ID"];
    //1. เชื่อมต่อ database: 
    include('connection.php');  //ไฟล์เชื่อมต่อกับ database ที่เราได้สร้างไว้ก่อนหน้าน้ี
    //2. query ข้อมูลจากตาราง plant: 
    $query = "SELECT * FROM plant WHERE PlantID = '".$ID."' ";
    //3.เก็บข้อมูลที่ query ออกมาไว้ในตัวแปร result . 
    $result = mysqli_query($conn, $query);
    while ($row = mysqli_fetch_array($result)) {
        echo "<div><p>PlantID : ".$row['PlantID']."</p></div>
                <div><p>PlantName: ".$row['PlantName']."</p></div>";
  
    }
    mysqli_close($conn);
    ?>
    <a href="showdata.php" class="btn btn-primary">กลับหน้าเดิม</a>
    </div>
    <br>
    <footer class="footer">
        <span> COPYRIGHT © 2020
            <a href="#" target="_blank">Soymilk</a>
            ALL Right Reserved
        </span>
    </footer>
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="assets/js/main.js"></script>
</body>
</html>